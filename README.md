# Hexis :dancer:

**Helix plus LaTeX for the humanities and social sciences.**

This is for all those LaTeX users in the humanities and social sciences just trying to figure stuff out.
I am one of them.
Here I share what I've learned.

![Example of a Helix+LaTeX workflow](https://gitlab.com/teunphil/hexis/-/raw/main/example.gif)*What my Helix+LaTeX workflow looks like.*

---

## Contents
1. [Introduction](#introduction)

2. [Features](#features)

3. [Reasons for using LaTeX](#reasons-for-using-latex)

4. [Reasons for using Helix](#reasons-for-using-helix)

5. [Getting started with Helix](#getting-started-with-helix)

6. [General Helix config](#general-helix-config)

7. [LaTeX-specific config](#latex-specific-config)

8. [Getting started with LaTeX](#getting-started-with-latex)

9. [Useful LaTeX packages](#useful-latex-packages)

10. [LaTeX tips & tricks](#latex-tips-tricks)

11. [Further resources](#further-resources)

---

## Introduction

[Helix](https://helix-editor.com/) is a wonderful text editor.
[LaTeX](https://www.latex-project.org/) is a marvellous piece of software.
Getting the two to work together though, can be challenging, especially for a beginner.
I am by no means an expert, but I have figured out a thing or two through trial and error.
This is a living document that aims to collect my accumulated skill and knowledge regarding writing LaTeX documents in Helix.

I don't actually recommend copying my entire setup (why would you? you're not me).
But I invite you to follow along and see what works for you and what doesn't – or, of course, to cherry-pick solutions to specific problems you're having.

Also, if you're like "Hey, I'm not a programmer, I just want to think about Dasein instead of using *code* to, like, *program* my *editor* or whatever", that's totally valid!
You can just use the LaTeX stuff and ignore the Helix stuff.
Or, of course, you could turn back to MS Word altogether and occasionally, when inserting an image has once again brought your entire system to a screeching halt, you look out the window and think about the beautiful documents you could be writing if you had just followed that random internet person's guide.
But you know, no judgement.

---

## Features
My setup features the following:

- On the editor side:
	- Automatic and continuous compiling ([↓](#setting-up-texlab))
	- Zero-config syntax highlighting
	- LaTeX diagnostics (TeXlab) and semantics feedback (ChkTeX) ([↓](#setting-up-texlab))
	- Spelling and grammar checking through LanguageTool ([↓](#setting-up-ltex))
	- Automatic forward search from Helix to PDF ([↓](#setting-up-texlab))
	- Pretty colours ([↓](#theme))
	- Dead easy setup (compared to Vim and Neovim…)

- On the output side:
	- Simple and elegant documents
	- Clickable links everywhere ([↓](#hyperref-hyperlinks))
	- Automatic references and bibliography ([↓](#biblatex-chicago-references-and-bibliography))
	- Support for multiple languages ([↓](#babel-internationalisation))
	- Two-column support ([↓](#two-columns))
	- Abstract and keywords ([↓](#abstract-and-keywords))
	- Subtitles, sub-headers, sub-everything ([↓](#subtitles))
	- Tables ([↓](#booktabs-tables)), images ([↓](#graphicx-images)), and appendices ([↓](#appendix)) if you're into them

Features it **doesn't** have and that I miss:

- LaTeX-specific motions and commands, akin to VimTeX for Vim (it's possible to set up manually but I'm currently ~too lazy~ busy with other stuff)
- Persistent word count in the statusline (currently not possible in Helix)

Features it **doesn't** have, and probably never will (because they don't cover my use case):

- Snippets
- Autocompletion
- File manager and related functions
- Extensive support for working on multiple files simultaneously
- A fully fledged IDE-like environment à la [SpaceVim](https://github.com/SpaceVim/SpaceVim) or [kickstart.nvim](https://github.com/nvim-lua/kickstart.nvim)

---

## Reasons for using LaTeX

LaTeX is different from your average word processor such as Word or Writer.
We call these [What You See Is What You Get (WYSIWYG)](https://en.wikipedia.org/wiki/WYSIWYG) editors, because, well, they let you visually edit your documents in real-time.
In contrast, LaTeX is a *language* in which you can specify the content and form of your document in a `.tex` file.
You can then compile this file into a proper document (usually a pdf) using a LaTeX engine.
(However, the workflow described here will abstract away a lot of this detail.)

This way of working has a few advantages:

### Precision and consistency
In LaTeX, what happens to your document is only what you tell it to.
This means that Word annoyances such as random typographic changes or that infuriating vaguely blue background behind pasted text are unthinkable.
LaTeX feels very robust, because you first specify what you want your document to look like (in the 'preamble'), and then you type out your document which will be formatted exactly how you specified it.
This is what people mean when they say that LaTeX "separates form from content".
I'm personally a big fan, because when I'm writing I don't care where exactly the line breaks or how this figure ends up on the page or how that footnote looks.
With LaTeX, I can trust that these things will sort themselves out, and I can focus on writing instead.

In Word or Writer, formatting information is often 'hidden': "why is this paragraph all underlined in red?
Ah, it's because the program thinks it's in a different language for some reason."
In LaTeX, every change, whether local or global, is declared explicitly.
So instead of applying a hidden option to a certain chunk of text, you write it out:

```latex
As Descartes said: ``\foreignlanguage{latin}{cogito, ergo sum}''.
```

The same goes for bold and italics, font size, spacing, colour, links, capitalisation, footnotes, all the way up to tables and bibliographies: everything is explicitly declared.
This makes unpleasant surprises almost impossible.

### Ａ Ｅ Ｓ Ｔ Ｈ Ｅ Ｔ Ｉ Ｃ Ｓ
If you make a new document in Word or Writer, use all the defaults and write a few sentences with a header, the result is… not dashing.
If you make a new LaTeX document and add the bare minimum, the result looks awesome and professional.
Beautiful, fully justified text in a classy font and with lovely titles and headers (and no blue!).
And with some small tweaks you can make it look even better!

### Referencing
In my experience, referencing to sources in Word is okay, but it feels a bit like a hack.
By contrast, referencing to sources in LaTeX is buttery smooth.
You tell it to use a certain style and all formatting happens automatically, including in the bibliography.
The bibliography itself can be called with a single command and is automatically populated with all referenced sources.

### Version control
`thesis.docx` `thesisfinal.docx` `thesisfinalFINAL.docx` `thesisREALLYFINALFORREAL.docx` `final_thesisREALLYFINALFORREAL.docx`

LaTeX allows you to completely avoid endless versions of the same document and streamline the version control process.
This is because LaTeX files are just plaintext files, and plaintext files are manageable using `git`.
If you've never used Git, it is another one of these Spartan-looking programs that is somewhat difficult to get into.
But the basics are easy to learn, and there are plenty of [beginners' guides](https://towardsdatascience.com/an-easy-beginners-guide-to-git-2d5a99682a4c).

If you use Git consistently, you'll have a snapshot of every previous state of your document which you can revert to if necessary.
It also allows you to keep track of different 'branches' (for example, for teachers' suggestions) which you can later merge into your main document.
And finally, Git allows for very smooth collaboration in groups.

---

## Reasons for using Helix

I have gone from Vim to Neovim to Helix for my LaTeX needs.
Both Vim and Neovim are very barebones after install, and they expose the user to [a lot of complexity](https://rawgit.com/darcyparker/1886716/raw/eab57dfe784f016085251771d65a75a471ca22d4/vimModeStateDiagram.svg).
They are designed to be tinkered with and tuned to the user's own wishes.

Helix, by comparison, is much more feature-complete out of the box, while providing a much less complex user experience.
This is a major draw: if you want a modular editor like Vim but don't want to spend hours going through config files, installing plugins to manage other plugins and (*shudder*) writing Lua code, then Helix will probably make you very happy.
(Though see some of the [differences](https://github.com/helix-editor/helix/wiki/Migrating-from-Vim) between Helix and Vim.)

It also comes with many of the advantages of (neo)vim: it is very efficient in its modularity, it is lightweight despite its rich default featureset, and it leaves the user in control. (And of course, it is fully open-source!)
It is not nearly as customisable as (neo)vim, especially as it currently still lacks plugin support, but for my money, I discovered that I need plugins much less than I originally thought.

---

## Getting started with Helix
[Install Helix](https://docs.helix-editor.com/install.html).

Run `helix --tutor` to get a walkthrough of its basic functionality.

There's **a lot** of hotkeys to make use of, but for beginners, I want to emphasize the following:

* `:w`, `:wq` and `:q!` – save ('write'), save and quit, and force quit without saving respectively.
* `space y` and `space p` – respectively copy ('yank') and paste **to and from the system clipboard**. Regular copy and paste will use a buffer-specific register, which means that it stays within your file and cannot travel to other files.
* `/` – search.
* `u` and `U` – undo and redo respectively.
* `gg` and `ge` – jump to the beginning and end of the file respectively.
* `%` – select entire file.
* `~` – toggle case of selected character.

### The structure of config files

On Unix systems, all the configuration will live sensibly in `~/.config/helix` (unlike *some* editors…).
(On Windows, the config folder is `%AppData%\helix`.)
The config file is called `config.toml` – create one if it doesn't yet exist.

Next to that, you can set language-specific configs in a separate file: `languages.toml`.
We'll cover the general config first, and then move to the LaTeX-specific stuff.

---

## General Helix config
You can do a lot of things in your config file, and I highly recommend you check out the [documentation](https://docs.helix-editor.com/configuration.html) for an extensive list of options.
However, this is my own config for comfy LaTeX editing:

```toml
theme = "kanagawa"

[editor]
cursorline = true
color-modes = true

[editor.statusline]
left = ["mode", "spinner"]
center = ["file-name"]
right = ["diagnostics", "position", "total-line-numbers", "file-encoding"]

[editor.lsp]
#display-messages = true

[editor.soft-wrap]
enable = true
wrap-indicator = ""

[editor.cursor-shape]
insert = "bar"
normal = "block"
select = "underline"

[keys.normal]
D = "kill_to_line_end"

[keys.insert]
"C-h" = "delete_word_backward" #most terminals that are not Alacritty, Kitty or Wezterm eat Ctrl-backspace and turn it into a Ctrl-h signal. So the 'C-h' here makes sure that Ctrl-backspace actually does what it should. (see https://github.com/helix-editor/helix/discussions/6971 and https://github.com/helix-editor/helix/wiki/Terminal-Support#enhanced-keyboard-protocol)
"C-del" = "delete_word_forward"
"C-left" = "move_prev_long_word_start"
"C-right" = "move_next_long_word_start"
```

### Theme
The most important part of any config!
Helix comes with [**many**](https://github.com/helix-editor/helix/wiki/Themes) themes built-in.
Selecting a built-in theme is as easy as adding `theme = "<theme_name>"` to the config file.
You can also [select a custom theme](https://github.com/helix-editor/helix/wiki/Themes#setting-theme) or [make your own](https://docs.helix-editor.com/themes.html#creating-a-theme).

### UI customization
I want the line of my cursor to be highlighted.
I also want a clear indication of the current mode (normal, insert, select, etc.).
Both of these options can be found under the `editor`-header of the configuration.
For the uninitiated, the various settings of Helix exist in a tree-like structure.
The 'folders' of this structure are separated by dots.
For instance, the category `editor` contains both the `cursorline` and `color-modes` options, which can be written like this:

```toml
editor.cursorline = true
editor.color-modes = true
```

However, to avoid unnecessary repetition, these 'folders' can also be indicated as headers in the config file.
For instance:

```toml
[editor]
cursorline = true
color-modes = true
```

The two ways of notating are functionally identical, and only differ in terms of aesthetics.
Note that the headers themselves can also contain dots.

### Statusline
The statusline is the strip at the bottom that gives you information about the document, the current mode, and the current location.
It has three sections: left, center and right.
By default, it shows the input mode, an activity indicator ('spinner') and the filename on the left.
On the right, it shows warnings and errors ('diagnostics'), the number of active selections, the cursor position, and the file encoding.
The center is empty by default.

However, all this can be customized!
The [docs](https://docs.helix-editor.com/configuration.html?highlight=statusline#editorstatusline-section) list all possible items that you can put here.
I have slightly tweaked the statusline for myself:

```toml
[editor.statusline]
left = ["mode", "spinner"]
center = ["file-name"]
right = ["diagnostics", "position", "total-line-numbers", "file-encoding"]
```

I've moved the filename to the center, I added the total number of lines to the right (useful when opening large LaTeX files) and I removed the number of active selections.

### Soft wrapping
'Soft wrapping' means that long lines will automatically wrap around to the next visual line so that text never falls off to the right.
This option is very useful for us LaTeX users, since some of our sentences will likely exceed the view width (especially if you give your compiled document 50% of your screen width, as I do).
Enabling it is dead easy:

```toml
[editor.soft-wrap]
enable = true
wrap-indicator = ""
```

I also turn off the wrap indicator, which is `↪` by default.
I think it's distracting, and unnecessary since the line numbers clearly show wrapped lines already.
(See the [docs](https://docs.helix-editor.com/configuration.html?highlight=soft%20wrap#editorsoft-wrap-section) for more options.)

### Cursor shape
By default, the cursor is always a block.
I find it intuitive to have it change to a bar in insert mode, and to an underline in select mode.

```toml
[editor.cursor-shape]
insert = "bar"
normal = "block"
select = "underline"
```

### Custom keymaps
Yes, you can make custom keymaps just like in Vim!
Granted, it's not as powerful, but it's also much simpler.
Check the [docs](https://docs.helix-editor.com/remapping.html) for an overview!
Oh, and [here](https://docs.helix-editor.com/keymap.html) are all the default keymaps.

You remap keys per mode, and instead of keypresses (like in Vim) you give commands.
Here's my remaps:

```toml
[keys.normal]
D = "kill_to_line_end"

[keys.insert]
"C-h" = "delete_word_backward" #most terminals that are not Alacritty, Kitty or Wezterm eat Ctrl-backspace and turn it into a Ctrl-h signal. So the 'C-h' here makes sure that Ctrl-backspace actually does what it should. (see https://github.com/helix-editor/helix/discussions/6971 and https://github.com/helix-editor/helix/wiki/Terminal-Support#enhanced-keyboard-protocol)
"C-del" = "delete_word_forward"
"C-left" = "move_prev_long_word_start"
"C-right" = "move_next_long_word_start"
```

In normal mode, I want to replicate Vim's `D` because I find it very useful.
In insert mode, I want to have word-wide motions when holding Ctrl (which already exists by default in a lot of software, from MS Word to VSCode to Firefox).
These latter ones are slightly tricky to get right in Helix, for two reasons.

1) `Ctrl-backspace` isn't properly sent on most terminals (Alacritty, Kitty and Wezterm are the lucky exceptions).
Instead, it's sent as `Ctrl-h`.
This means that in order to map `Ctrl-backspace` to something, we have to use `Ctrl-h` (or `C-h` in Helix-speak).

2) 'Going to the end of the next word' doesn't really work as expected in Insert mode (for… [reasons](https://github.com/helix-editor/helix/issues/3395)), so we have to settle for the next best thing: going to the _start_ of the next word. It's fine, but it's not _exactly_ how most other software handles `Ctrl-right`.

---

## LaTeX-specific config
Setting up LaTeX-specific options amounts to telling the editor how to handle the LaTeX language.
This means we have to move to the `languages.toml` file.
Here, you can set headers for individual languages (between double square brackets), and then set all the options for that language.
This file has the same structure as the general config file: nested 'folders' of options.

I'll get straight to the point and dump mine here:

```toml
[[language]]
name = "latex"
language-servers = [ 
  { name = "texlab" },
  { name = "ltex" }
]

[language-server.texlab]
command = "texlab"

[language-server.texlab.config.texlab]
build = { onSave = true, forwardSearchAfter = true, executable = "latexmk", args = ["-interaction=nonstopmode", "-synctex=1", "%f", "-pdflua"] }
forwardSearch = { executable = "zathura", args = [ "--synctex-forward", "%l:1:%f", "%p" ] }
chktex = { onEdit = true }

[language-server.ltex]
command = "ltex-ls"

[language.auto-pairs]
"`" = "'"
```

### Setting up LaTeX
The general settings for the LaTeX language here are nothing more than giving the name and pointing Helix to the two language servers I want to use: TeXlab and lTeX.
These are configured below.

### Setting up TeXlab
[TexLab](https://github.com/latex-lsp/texlab) is a great language server that doesn't only provide diagnostics, but also auto-compile features (like Vimtex on Vim).
It basically turns Helix into something of a LaTeX client!
To use it:

1) Install TexLab on your system. Check on [repology](https://repology.org/project/texlab/versions) whether your Linux distro provides it, and if not, [build it from source](https://github.com/latex-lsp/texlab#building-from-source);

2) Point Helix to it under the `[[language]]` header (see previous section);

3) Specify the command to use.
This is simply `texlab`, and you specify it under the `[language-server.texlab]` header.
This is the only Helix-side config that I set for TeXlab;

4) Provide config options under the header `[language.config.texlab]`.
These are simply the options that Helix will pass onto TexLab.
You can find all possible options [here](https://github.com/latex-lsp/texlab/wiki/Configuration).

Most important are the build options (`build = { … }`).
Most most importantly, I want TexLab to auto-compile my documents on save (`onSave = true`).
I also want it to automatically perform a forward search, so that the PDF jumps to the place of the cursor in Helix (`forwardSearchAfter = true`).
The executable should be `latexmk` (this is actually the default, but I keep it in my config for clarity).
And finally, I supply the arguments that should be passed on to latexmk.
The only change I make from the [defaults](https://github.com/latex-lsp/texlab/wiki/Configuration#texlabbuildargs) is leaving out the `-pdf` flag, because I _don't_ want to it run `pdflatex`, but `lualatex` (this also requires a small edit to `latexmkrc`, see [Changing TeX engines](#changing-tex-engines)).
If you do want to use `pdflatex` (or if you have no idea what these words mean – in that case, you're using `pdflatex`), you can just leave the latexmk arguments to their defaults by leaving them out of this config altogether.

Next, I set up forward search (`forwardSearch = { … }`).
I set the executable (i.e. what PDF viewer to open your compiled documents in) to `zathura` because I like it.
You can set it to whatever you like, but make sure that your viewer supports SyncTeX (Okular, Evince, SumatraPDF, qpdfview and Skim all do; see the [docs](https://github.com/latex-lsp/texlab/wiki/Previewing)).
Then I provide the arguments passed on to the pdf viewer: `args = [ "--synctex-forward", "%l:1:%f", "%p" ]` (I'll be honest, I have no idea where I got this, but it works!)

Finally, I want TexLab to give me diagnostics on LaTeX semantics (such as using the correct dashes and spaces everywhere).
This can be easily achieved by telling TexLab to run `chktex` after any edit: `chktex = { onEdit = true }`.
[ChkTeX](https://www.nongnu.org/chktex/) is pretty great and gives lots of useful warnings ([here's all of them](https://www.nongnu.org/chktex/ChkTeX.pdf)).

### Setting up LTeX
Enabling LTeX is really simple!
The steps are pretty much the same as for TexLab:

1) Install LTeX on your system.
Check on [repology](https://repology.org/project/ltex-ls/versions) whether your Linux distro provides it (pretty much only Arch and NixOS at this point), and if not, follow [these instructions](https://valentjn.github.io/ltex/ltex-ls/installation.html) for a manual install;

2) Point Helix to it under the `[[language]]` header (see previous section);

3) Specify the command to use.
This is simply `ltex-ls`, and you specify it under the `[language-server.ltex]` header.

That's it!
You can add a bunch of settings if you want ([here's all of them](https://valentjn.github.io/ltex/settings.html)), but I don't set any config options for now.
It works just fine the way it is!

### Setting up auto-pairs
And finally finally, away from the world of language servers, you can set up auto-pairs, meaning that for certain pairs (e.g. `()`, `[]`, `{}`) Helix automatically inserts the second after you type the first.
For LaTeX, the only auto-pair I find useful is quotation marks.
So I've set it up so that whenever I type an opening quotation mark, I get a closing one.
This also works well for double quotes, which is pretty neat.

```toml
[language.auto-pairs]
"`" = "'"
```

Let's try to get an overview of what we've set up here.

When you open a `.tex` file in Helix, Helix will run TexLab.
TexLab will run quietly in the background, and whenever you make an edit to your document, it will run ChkTeX and show you the results in the Helix UI.
Also, LTeX will be checking your document on every edit, pointing our embarrassing spelling and grammar mistakes.
Whenever you save your document, TexLab will run `latexmk` in order to compile it.
Latexmk will run whatever TeX engine it has been set up with (i.e. `pdflatex`, `lualatex`, etc.).
Then, TexLab will open Zathura with forward search enabled, so that your document is opened at the correct location.

---

## Getting started with LaTeX

Honestly, the best tip I can give any new LaTeX user is to open up [Modern LaTeX](https://github.com/mrkline/modern-latex) and go through chapters 2–4.
That will teach you about installing LaTeX, the basics of writing in LaTeX, and LaTeX document structure better than I ever could.

**→ NOTE:** if you use [TeXlab](#setting-up-texlab), you don't have to manually compile your documents with `$ xelatex hello.tex`; your documents will automatically compiled upon saving (though learning it the manual way cannot hurt).

---

## Useful LaTeX packages

### fontenc

**→ NOTE:** Only if you use pdfLaTeX (for other TeX engines, see [fontspec](#fontspec))

```latex
% Set font encoding to T1
% Comment to restore to LaTeX default (OT1)
% If you have no idea what this is, it's best to leave it
\usepackage[T1]{fontenc}
```

This is really just a commonsense setting to bring LaTeX font encoding into the 21st century.
You can read more about it [here](https://tex.stackexchange.com/questions/664/why-should-i-use-usepackaget1fontenc).

### fontspec

**→ NOTE:** Only if you use LuaLaTeX / XeLaTeX

XeLaTeX and LuaLaTeX use `fontspec` in order to load system fonts.
This means that instead of loading a specific font package, you load `fontspec` and specify the font family you want to use.

```latex
\usepackage{fontspec}
```

### tgtermes (font)

Fonts are very subjective, but I like Tex Gyre Termes.
If you don't, look for other fonts [here](https://tug.org/FontCatalogue/seriffonts.html) (or hell, just leave it on the default, Computer Modern).

**For fontenc**

```latex
\usepackage{tgtermes}
```

**For fontspec**

```latex
\setmainfont{TeX Gyre Termes}
```

#### Fonts troubleshooting
If your fonts are acting up, make sure that you have the right TeXlive packages installed!
Not having the right packages can give you errors that might just take hours and hours of your time (ask me how I know).
For example, on Arch Linux, you need `texlive-fontsrecommended` for most fonts to work.
When in doubt, you can always install the entirety of TeXlive to check if it's a packages problem.
If the problem persists, [double-check](#checking-tex-engines) what engine you're using.

### babel (internationalisation)

If you're not writing in American English, `babel` takes care of pretty much all language-related settings for you.
You can give it the language directly by invoking `\usepackage[dutch]{babel}`, but I prefer doing it as follows:

```latex
\documentclass[dutch]{article}

% Sets document language based on the argument passed to the \documentclass variable
\usepackage{babel}
```

This way, any other packages that take a language as an argument also know that your document is in Dutch.

### graphicx (images)
`graphicx` allows you to add images as figures.

```latex
\usepackage{graphicx}
\graphicspath{{images}}
```

To add an image, you can use the following template:

```latex
% By default, figures appear in the most space-saving spot.
% You can change this behaviour by specifying an option between brackets after \begin{figure}:
%	'h' puts them (approximately) where you assign them.
%	't' puts them at the top.
%	'b' puts them at the bottom.
%	'p' puts them on their own page.
\begin{figure}[h]
	\begin{center}
		% You can specify the size of the image by setting 'width=' or 'height='
		% The size can be set absolutely (in px, cm, etc.), relatively (with 'scale=1.5') or relative to \textwidth, like done below
		\includegraphics[width=0.8\textwidth]{image.png}
		\caption{A very beautiful image.}
		\label{fig:beautiful}
	\end{center}
\end{figure}
```

### biblatex-chicago (references and bibliography)

BibLaTeX manages your references and bibliographies.
This particular flavour does so in the [Chicago](https://www.chicagomanualofstyle.org/tools_citationguide.html) style, a popular style in the humanities and social sciences.

→ **NOTE:** If you require a different citation style, you should do some research to see whether you should use plain `biblatex` or a custom version of it.
It seems that for MLA you can use [biblatex-mla](https://ctan.org/pkg/biblatex-mla), and for APA there is [biblatex-apa](https://ctan.org/pkg/biblatex-apa) (both have been updated in 2022), but please do some [Ducking](https://duckduckgo.com/) before sticking to a method.

In any case, I invoke `biblatex-chicago` as follows (this should work with any biblatex package):

```latex
% Enables BibLaTeX-Chicago in the author-date format, with Biber as backend, and suppressing all doi's and isbn's
% Biber needs to be installed on your machine for this to work!
% Change 'authordate' to 'notes' to use footnotes instead
\usepackage[
authordate,
backend=biber,
doi=true,
isbn=true,
cmsdate=both]
{biblatex-chicago}

% Sets the bibliography source file
% Takes both absolute and relative paths, so all of the following are valid:
%	/home/name/Documents/article/sources.bib
%	sources.bib
%	../sources/sources.bib
\addbibresource{filename.bib}

% Enables csquotes, which is recommended for BibLaTeX-Chicago
\usepackage[autostyle=true]{csquotes}
```

Here, I tell it to use the author-date style, to use Biber as its back-end, to include DOI's and ISBN's in the bibliography, and to include the original date of a work if specified (through `tex.origdate: <date>`).
Then, I point the package to my `.bib` file.
And lastly, I enable `csquotes`, which is recommended by BibLaTeX.

#### Referencing
There's a few commands that I use the most when referencing sources.
In each of the following commands, the label of the referenced source will be `simard21`:

- `\autocite{simard21}` — author + year in parentheses
- `\autocite*{simard21}` — only the year in parentheses
- `\textcite{simard21}` — author in text, year in parentheses
- `\citeauthor{simard21}` — author in text (not an actual citation)
- `\citetitle{simard21}` — title in text (not an actual citation)

You can put stuff before and after the citation with square brackets:

```latex
In science, women are often ridiculed when they show an affinity with nature \autocite[see for instance][206]{simard21}.
```

If you use only one pair of square brackets, BibLaTeX assumes that it goes *after* the citation:

```latex
In science, women are often ridiculed when they show an affinity with nature \autocite[206]{simard21}.
```

If you only want to put something in front of a citation, use a second empty pair of brackets:

```latex
In science, women are often ridiculed when they show an affinity with nature \autocite[see for instance][]{simard21}.
```

*(This is not LaTeX-related, but **please** use en dashes (`--`) for page ranges. Please, do it for me.)*

The nice thing about BibLaTeX' citation commands is that they're context-aware: if you cite the same source multiple times consecutively on the same page (because you're citing different pages, for instance), BibLaTeX will automatically omit author and year from the latter references.

If you want to force BibLaTeX to show the full citation anyways, use `\citereset` before the citation you want to force-show.
For instance:

```latex
\Textcite{bridle22} cites multiple artistic renderings of technological ecologies.
Take for instance the following snippet: ``a cybernetic forest / filled with pines and electronics / where deer stroll peacefully / past computers / as if they were flowers / with spinning blossoms'' \citereset\autocite[Richard Brautigan, cited in][]{bridle22}
```

#### Automatic citereset in new (sub)section
You can tell BibLaTeX to automatically do a `citereset` after every new section or subsection.
To do this, add `citereset=section+` or `citereset=subsection+` to the BibLaTeX options.
For mysterious reasons, it does not support subsubsections.

#### Only show URL if no DOI
With the above settings, BibLaTeX will *always* show a DOI (if available), and *never* a URL.
However, if you're like me, then you want to show a URL in certain cases, such as when citing a video or blog.
But you don't want to show a URL if you're already showing a DOI, because that's unnecessary and messy.
And you could just delete URLs from entries that already have a DOI, but that's wasteful and time-consuming and you don't really want to adapt your source data to achieve proper LaTeX formatting.

Well, there's a solution!

First, set both `doi` and `url` to `true` in the BibLaTeX settings:
```latex
\usepackage[
authordate,
backend=biber,
doi=true,
isbn=true,
url=true,
cmsdate=both,
citereset=subsection+]
{biblatex-chicago}
```

Then, add this snippet to your preamble, *under* the BibLaTeX invocation: ([source](https://tex.stackexchange.com/a/424799))

```latex
\DeclareSourcemap{
  \maps[datatype=bibtex]{
    \map[overwrite]{
      \step[fieldsource=doi, final]
      \step[fieldset=url, null]
      \step[fieldset=eprint, null]
    }  
  }
}

```

That's it!

#### Reference management
I personally maintain my `.bib` file with with the open-source [Jabref](https://www.jabref.org/).
It works directly with `.bib` files and doesn't create any further cruft (like Zotero does), so it's highly portable.

As for the sources themselves, I would advise against using [Sci-Hub](https://sci-hub.ru/) (for articles) and [LibGen](https://libgen.rs/) (for books), as well as [Anna's Archive](https://annas-archive.org/) (for everything).
Because how can Elsevier ([>$1.6 billion](https://www.relx.com/~/media/Files/R/RELX-Group/documents/reports/annual-reports/relx-2022-annual-report.pdf) net profit in 2022) ever survive if they cannot charge students paywall money for each article?

### hyperref (hyperlinks)

Hyperref enables hyperlinks in your documents.

```latex
\usepackage{hyperref}
% Set custom colors for urls, links and citations
\hypersetup{
  colorlinks   = true, %Colours links instead of ugly boxes
  urlcolor     = blue, %Colour for external hyperlinks
  linkcolor    = red, %Colour of internal links
  citecolor   = blue %Colour of citations
}

% The 'caption' package enables the [hypcap=true] option (enabled by default), which lets hyperref refer to the figure itself instead of the caption
\usepackage{caption}
```

The only configuration I do here is some visual tweaks, because the default look of links is… not great.
The `caption` package simply fixes an issue where clicking a link to a figure jumps the pdf to the caption instead of the figure itself.

### cleveref

Cleveref is a way to make internal references a bit easier.
In particular, Cleveref 'knows' what kind of thing you're referring to (a figure, a section, a page), so that it's easier to fit internal references into a sentence.

```latex
% Enables Cleveref, which partly automates internal references
% Instead of 'see paragraph \ref{sec:analysis}', write 'see \cref{sec:analysis}'
% Cleveref will automatically append the type of thing you're referencing (e.g. paragraph, figure, appendix, etc.) and do so in the right language
\usepackage{cleveref}
```

### booktabs (tables)

Booktabs makes it easy to create pretty and clear tables.
Specifically, the package follows [a set of rules](https://ftp.snt.utwente.nl/pub/software/tex/macros/latex/contrib/booktabs/booktabs.pdf) for making what they consider 'good tables'.
I agree with them, so I use it.

```latex
% Making tables in LaTeX can be done without any extra packages
% However, booktabs makes tables simple and pretty
\usepackage{booktabs}
```

You can then make a table like this:

```latex
% By default, this table is put in a floating figure.
% To put this table in-line instead, comment the lines \begin{figure}, \caption, \label and \end{figure}
\begingroup % A group is created to quarantine the distance parameters set below
% The two lines below increase the distance between columns (\tabcolsep) and between rows (\arraystretch).
\setlength{\tabcolsep}{9pt} % Default LaTeX value: 6pt
\renewcommand{\arraystretch}{1.5} % Default LaTeX value: 1
\begin{figure}
% This table has two paragraphs that wrap their text automatically (hence the p's).
% Besides 'p', other options include c (center), l (left align) and r (right align).
\begin{tabular}{p{0.45\textwidth} p{0.45\textwidth}} \toprule
	\textbf{Header 1} & \textbf{Header 2} \\
	\midrule
	Some text on the left. & Some more text on the right. \\
	This is a somewhat longer though not unreasonably long piece of text. & This is short. \\
	Etc. etc. & You probably get the point by now. \\
		\bottomrule
\end{tabular}
\caption{A very simple example table.}
\label{fig:table}
\end{figure}
\endgroup
```

### pdfpages (external PDF's)

pdfpages lets you include (parts of) PDF files in your document.

```latex
% Insert PDF's
\usepackage{pdfpages}
```

Insert a file like this:

```latex
% The file will be automatically be given its own page, no need to do a \pagebreak before or after
\begin{figure}[h]
    \centering
% If you only want to include certain pages from a document, write them in the first set of curly braces
% Write the filename without extension in the second set of curly brackets
    \includepdf[pages={-}]{frontpage}
\end{figure}
```

### microtype

Microtype makes a bunch of small typesetting changes that make a small but perceptible difference in how your document looks.
It is designed to work its magic out-of-the-box, and newbies (like me) are best off leaving it on its default behaviour.

```latex
% Microtype makes a bunch of small typesetting changes to make your document look better
% Comment to speed up compiling, or to see what the differences are
\usepackage{microtype}
```

### geometry (paper size)

Set the dimensions of your paper.
I personally am too European to know what 'letter paper' even is, so I set mine to A4.

```latex
% Options: a4paper, letterpaper (LaTeX default)
\usepackage[a4paper]{geometry}
```

### setspace (spacing)

We all know that double spacing looks terrible, but sometimes an assignment requires you to use it.
Setspace has you covered.

```latex
\usepackage{setspace}
% Options: singlespacing (default), onehalfspacing, doublespacing
\onehalfspacing
```

### todonotes
When working in bigger documents, I sometimes want to flag stuff so that I can come back to it later.
For this I use the package `todonotes`:

```latex
\usepackage{todonotes}
```

It's very simple: you make to-do's with `\todo` and you can insert a list of all of them with `\listoftodos`

```latex
It is a commonly known fact that global warming is a myth.\todo{Find a source for this.}

\listoftodos{}
```

### comment

This is just a convenience package, allowing you to comment out large portions of text with the `comment` environment.
Bonus: it works perfectly with Helix' treesitter-powered syntax highlighting, which will correctly grey out comment blocks!

```latex
% Enables comment blocks
\usepackage{comment}

\begin{comment}
This is all commented out.
And so is this!
\end{comment}
```

---

## LaTeX tips & tricks

### Counting words
[TeXcount](https://app.uio.no/ifi/texcount/index.html) is part of TeXlive and therefore should already be installed on your system.
As far as I know, its counts are fairly accurate, with only the bibliography not being counted.

To get a detailed word count per section, simply go to the folder where your `.tex` file resides and run `texcount <filename>` in the terminal.

To get a simple one-figure count for your whole document straight in Helix, run `:sh texcount -0 -sum <filename>`.

`:sh` tells it to run a shell command.
The `-0` and `-sum` flags reduce the output to one single number.

The result should pop up in the editor.

### A properly formatted table of contents

If you simply plop down your table of contents, you might have a number of problems:

1) it's too widely or narrowly spaced;
2) it's completely red because of hyperref;
3) it includes all the subsubsections and is now three pages long.

Let's solve all these problems at once:

```latex
{ 
\onehalfspacing
\hypersetup{linkcolor=black} %To make the links in the ToC black instead of red
\setcounter{tocdepth}{2} % To include only sections (level 1) and subsections (level 2) in the ToC
\tableofcontents
}
```

### Abstract and keywords

LaTeX has a separate environment for abstracts, creatively named `abstract`.
Keywords can be formatted manually.

```latex
\begin{abstract}
Bla.
\end{abstract}

\textit{\textbf{\small Keywords --- }\small first, second, third, etc.}
```

### Subtitles

Add a subtitle to your document by making the title bold and adding a line break.

```latex
\title{\textbf{A neat title}\\A neat subtitle}
```

### Subheaders (and table of contents)

If you want to use the above trick with section headers, you run into the problem of having line breaks in your table of contents (since the table of contents literally copies the entire header).
The solution is to provide a separate ToC header in square brackets.
For example:

```latex
\section[Instrumental reason: Horkheimer's Eclipse of Reason]{Instrumental reason\\ {\large Horkheimer's Eclipse of Reason}}
```

### Appendix

When adding an appendix, you probably want a number of things:

1. Starting on a new page;
2. Resetting page numbering;
3. A different page numbering style;
4. Resetting section numbering;
5. A different section numbering style:
6. A separate bibliography with a custom title and a smaller header;
7. A proper place in the table of contents.

Fortunately, the macro `\appendix` takes care of all points except 3 and 6.

The page numbering style can be changed with the `\pagenumbering` command, and the separate bibliography can be arranged with the `refsection` environment.
The custom title and header can be defined after the `\printbibliography` command.

This is what it looks like:

```latex
% This adds an appendix to the document which is separate from the rest of the document, with a separate bibliography.

% Enables separate numbering for the appendix
% Options: arabic, roman, Roman, alph, Alph
\pagenumbering{roman}

% Creates a separate section of references for the appendix bibliography
\begin{refsection}

\appendix

% Write your body text here as you normally would, including chapters, sections, subsections, etc.
\section{First appendix}

% Prints the appendix bibliography with a smaller heading and custom title
\printbibliography[heading=subbibliography,title=Bibliography Appendix A]

\end{refsection}
```

### Every section on new page

Putting every section on its own page automatically requires defining a new command:

```latex
% Start a new page before every section
\let\stdsection\section
\renewcommand\section{\clearpage\stdsection}
```

### Multiple languages

If you're using multiple languages in one document, step 1 is to tell `babel`:

```latex
\usepackage[main=dutch,english]{babel}
```

Then, whenever you need to switch to your secondary language, use the `otherlanguage` environment:

```latex
Stroofwafels zijn slecht voor je.

\begin{otherlanguage}{english}
	``To be, or not to be, that is the question.''
\end{otherlanguage}

Deze tekst slaat nergens op.
```

This way, your spell/grammar checker won't freak out, and hyphenation will be correct.

### Two columns

Switching to two columns is as easy as typing `\twocolumn`.
However, in a document which consists fully or mostly of two columns, you should add the option `twocolumn` to the `\documentclass` command (see [this thread](https://tex.stackexchange.com/questions/332120/what-is-the-difference-between-twocolumn-and-documentclasstwocolumnbook) for some reasons).

Apart from that, I slightly tweak the default margins based on my own preferences.

```latex
\documentclass[twocolumn]{article}

%%% Margins %%%
% The default column separation is too small for my taste, so I slightly increase it here
% Comment the three commands below to revert the margins to their LaTeX defaults
% If you're curious about what these commands do to the layout exactly, add \addpackage{layout} to the preamble, and \layout to the main text to see a visual representation

% Increase the column separation by 10pt
\addtolength{\columnsep}{10pt}
% Compensate by pushing the whole text 5pt left
\addtolength{\hoffset}{-5pt}
% Compensate by adding 10pt to the total text width, effectively pushing both columns 5pt outward
\addtolength{\textwidth}{10pt}
```

Switch to one-column mode with `\onecolumn`, and back with `\twocolumn`.

#### Full-width figure in two-column mode
If you insert a regular figure while in two-column mode, it will only span a single column.
For a figure to take up the full two columns, simply add an asterisk, like so:

```latex
\begin{figure*}[h]
	\begin{center}
		% You can specify the size of the image by setting 'width=' or 'height='
		% The size can be set absolutely (in px, cm, etc.), relatively (with 'scale=1.5') or relative to \textwidth, like done below
		\includegraphics[width=0.8\textwidth]{image.png}
		\caption{A very beautiful image.}
		\label{fig:beautiful}
	\end{center}
\end{figure*}
```

### Draft and final version
There's a `draft` option that enables certain nice features for draft versions of your document.

```latex
\documentclass[draft]{article}
```

Firstly, it highlights all overfull lines with a black box.

And secondly, it plays nicely with a bunch of packages:

* **graphicx** ([↑](#graphicx-images)) – draws empty frames instead of images
* **hyperref** ([↑](#hyperref-hyperlinks)) – disables all linking (I personally dislike this, so I add the `final` option to the package)
* **pdfpages** ([↑](#pdfpages-external-pdfs)) – prints an empty box instead of the external file
* **todonotes** ([↑](#todonotes)) – if the `obeyDraft` option is enabled, shows ToDo notes **only** in draft mode
* **microtype** ([↑](#microtype)) – if the `disable=ifdraft` option is enabled, disables microtype in draft mode (can significantly improve compilation time!)

If you don't like the effect that `draft` has on a specific package, you can always add `final` to the options of that package.

To export a final version, change the `draft` in the documentclass options to `final`.

### Specific typesetting stuff

#### manual hyphenation
If certain words aren't hyphenated to your liking, for whatever reason, you can fix it manually.
In your preamble, add a `\hyphenation{}` command, and specify each word with hyphens at every possible hyphenation spot.

For example:

```latex
\hyphenation{neu-ro-phe-no-me-no-lo-gy}
```

#### dashes
`--` makes an en dash (–).

`---` makes an em dash (—).

#### non-breaking spaces
So `~` makes a non-breaking space.

When to use a non-breaking space?
I've gathered the following list:

* between number and its unit (e.g. `10~mm`);
* before an en-dash that indicates a break (e.g. `the guy~-- who was very annoying~-- kept staring at me`);
* in any situation where a distracting orphan appears:
    * after 'I' (e.g. `I~am`);
    * in references to parts of a document (e.g. `Chapter~12`, `Figure~3`);
    * more generally between [thing] and [number] (e.g. `step~1`; `plan~B`);
    * when a symbol is a tightly bound object of a preposition (e.g. `increase x by~1`; from `0 to~10`);
    * when enumerating in a paragraph (e.g. `this shows that the situation is 1)~untenable and 2)~preposterous`).

#### interword spacing
LaTeX thinks that a period should always be followed by a longer space.
This is usually correct, but not after abbreviations such as `e.g.` and `i.e.`
Tell LaTeX to use a regular space ('interword spacing') by escaping the space with a slash: `e.g.\ two kilograms of flour`

#### intersentence spacing
Even more rarely, LaTeX thinks that something is an abbreviation when it is really just the end of a sentence.
This happens only if a sentence ends with a capital letter.
To make sure that intersentence spacing is still applied, use `\@`.
For instance: `I do not like the WAIS-IV.\@ It is a terrible measure of general intelligence.`

#### spacing for nested quotations
Now we're really getting into the weeds.
When you have a quote within a quote, you can end up with triple quotation marks that don't look great in the final document.
To separate them manually, you can use a non-breaking space called a `kern`.
You can decide on the width, but I feel like 0.1 em is just right (recommended [here](https://tex.stackexchange.com/questions/320775/what-is-the-correct-way-to-handle-multiple-different-adjacent-quotes#comment783920_320775)):

```latex
Hence, the question of life is ``\kern0.1em`How could a mere physical system perform these complex functions?', not `Why are these functions accompanied by life?'\kern0.1em''
```

There's also an automatic way: the `csquotes` package.
To make this work, load the package and put your quotations in the `\enquote` command.
`csquotes` will automatically handle nested quotes, and do so language-appropriately (via `babel`).
Check the [docs](https://mirror.lyrahosting.com/CTAN/macros/latex/contrib/csquotes/csquotes.pdf) for more info!
(I don't use this method myself because I'm too used to using ``` `` ``` and `''` for my quotations.)

#### ellipses
`\dots` makes three dots, also known as an ellipse: `…`

#### non-breaking hyphens
`\babelhyphen{nobreak}` makes a non-breaking hyphen.
I've used this once in my whole academic career.

### pdfLaTeX vs. XeLaTeX vs. LuaLaTeX
XeLaTeX and LuaLaTeX are newer TeX engines compared to the much older pdfLaTeX, which is still the de facto standard.

For factual comparisons, see [texfaq](https://texfaq.org/FAQ-xetex-luatex) and appendix A of [Modern LaTeX](https://github.com/mrkline/modern-latex).
For discussions, see [this](https://tex.stackexchange.com/questions/605137/in-2021-does-anything-beat-pdflatex), [this](https://old.reddit.com/r/LaTeX/comments/cj0j8h/pdflatex_or_xelatex/) and [this](https://old.reddit.com/r/LaTeX/comments/x9acv3/which_would_you_recommend_xelatex_or_lualatex/).

As you can see, opinions are divided, and recent in-depth discussions are few and far between.

As for myself, using LuaLaTeX feels like future-proofing my workflow, and its support for system fonts is a nice plus (though XeLaTeX does this too).

### Changing TeX engines
If you want to use LuaLaTeX as a TeX engine instead of pdfLaTeX, you'll have to change this in the config for latexmk, which is the program that TexLab invokes to compile documents.
(You *can* also configure this in `languages.toml` by adding `-pdfxe` or `-pdflua` to `language.config.texlab.build.args`.
However, I prefer setting it system-wide in `latexmkrc` because then *all* invocations of latexmk use the same engine.)
On Linux, open `~/.config/latexmk/latexmkrc` (chances are you have to make the folder and file first), and add the following line ([source](https://github.com/mgeier/homepage/issues/7#issuecomment-930542435)):

```conf
$pdf_mode = 4; # LuaLaTeX
```

For XeLaTeX, add this ([source](https://tex.stackexchange.com/questions/27450/how-to-make-latexmk-work-with-xelatex-and-biber)):

```conf
$pdflatex="xelatex %O %S";
```

For the why of using a different engine, go [here](#pdflatex-vs-xelatex-vs-lualatex).

### Checking TeX engines
You can check which TeX engine is used for document compilation by loading the `iftex` package:

```latex
\usepackage{iftex}

% Uncomment one of the following:
%\RequirePDFTex{}
%\RequireLuaTeX{}
%\RequireXeTeX{}
```

Uncomment the line with the engine you want to check.
If the correct engine is used, the document will compile as normal.
If not, you will get an `Emergency stop` error.

It's good practice to leave this in your documents to make sure you're always using the correct engine.

---

## Further resources

### LaTeX
- [Modern LaTeX](https://github.com/mrkline/modern-latex) – If you're looking for a thorough yet no-nonsense guide to *actually using* LaTeX, this is probably the guide you're looking for.
- [Overleaf's 30-minute introduction](https://www.overleaf.com/learn/latex/Learn_LaTeX_in_30_minutes) – Concise walkthrough for beginners.
- [Blog post: LaTeX for the humanities](https://www.overleaf.com/blog/636-guest-blog-post-latex-for-the-humanities) – Excellent writeup on why to use LaTeX in the humanities.
- [LaTeX for Philosophers](https://tanksley.me/latex-for-philosophers/index) – *Very* thorough LaTeX tutorial geared towards philosophy and the humanities in general. (disclaimer: I still need to go through this one myself.)
- [Getting to Grips with LaTeX](https://www.andy-roberts.net/writing/latex) – 12-step LaTeX walkthrough that starts at the very beginning. Great for those who are new to LaTeX!
- [LaTeX-doc-ptr](https://mirrors.evoluso.com/CTAN/info/latex-doc-ptr/latex-doc-ptr.pdf) – A great list of general LaTeX recommendations.

### Helix
- The Helix tutor. Simply type `helix --tutor` in a terminal and follow the instructions.
- The built-in command palette. Hit `space ?` in Helix to fuzzy-search for commands.
- [The documentation!](https://docs.helix-editor.com/) It's good! Use it!
